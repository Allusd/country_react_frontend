import React, { Component } from 'react';
import {Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';

import './App.css';

import Frontpage from './components/Pages/Frontpage'
import About from './components/Pages/About'


class App extends Component {


  render() {
    let content = (
      
        <BrowserRouter>
          <React.Fragment>
              <Switch>
                <Redirect from="/" to="/Frontpage" exact />
                <Route path="/frontpage" component={Frontpage}/>
                <Route path="/about" component={About}/>
              </Switch>
          </React.Fragment>
        </BrowserRouter>
      
    );
    return content;
}
}

export default App;
