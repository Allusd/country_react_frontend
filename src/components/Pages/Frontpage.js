import React, { Component } from 'react';

import CountrySelect from '../../components/Select/CountrySelect';
import Country from '../../components/TableSummary/Country';
import NavigationBar from '../../components/NavigationBar/NavigationBar'
import Footer from '../Footer/Footer'
import './Frontpage.css'
class Frontpage extends Component {
  state = {
    CountryCode: "AFG",
    CountryName: "Afghanistan"  
  };

  sideHandler = side => {
    this.setState({ side: side });
  };

  countrySelectHandler = event => {
    const countryCode = event.target.value;
    this.setState({ CountryCode: countryCode });

  };


  render() {
    let content = (
      <React.Fragment>
        <NavigationBar/>
          <div className="front__wrapper">
            <CountrySelect
              selectedCount={this.state.CountryCode}
              onCountSelect={this.countrySelectHandler}
            />
            <Country selectedCount={this.state.CountryCode} />
          </div>
          <Footer/>

      </React.Fragment>
    );
    return content;
}
}

export default Frontpage;
