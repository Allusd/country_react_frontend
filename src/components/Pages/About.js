import React, { Component } from 'react';
import './About.css'
import NavigationBar from '../NavigationBar/NavigationBar'

class About extends Component {

  render() {
    let content = (
  
      <React.Fragment>
        <NavigationBar/>
        <div className="wrapper">
          <div className="content">

              <div className="basic_info_wrapper">
                <div className="basic_info">
                  <h1>Aleksi Leppimaa</h1>
                  <p>Programming student</p>
                  <p>Vantaa, Finland</p>
                  <p>Metropolia school of applied sciences - Tieto- ja Viestintätekniikka</p>
                </div>
                
              </div>

              <hr/>

              <div className="skills_languages">
                <div className="info__skills">
                  <h3>My primary programming languages</h3>
                  <div >
                    <p>Javascript, HTML, CSS</p>
                  </div>
                  <div >
                    <p>SQL, Graphql</p>
                  </div>
                  <div >
                    <p>Node, Express, React</p>
                  </div>
                </div>
                
                  <div className="info__languages">
                    <h3>Languages</h3>
                    <div>
                      <p>Finnish</p>
                    </div>
                    <div>
                      <p>English</p>
                    </div>
                    <div>
                      <p>Swedish</p>
                    </div>
                  </div>
                  
            </div>
            <div className="clearer"></div>
            <hr/>

              <div className="work__school_info">
                <div className="work__info">
                  <h2>Hobbies</h2>
                  <div>
                    <h3>Gym</h3>
                    <p>Going to the gym with my father about 3 times a week.</p>
                  </div>
                  <div >
                    <h3>Gaming</h3>
                    <p>Playing games alone and with friends. Pc and board games :).</p>
                  </div>
                </div>
                
                <div className="school__info">
                  <h2>Education</h2>
                  <div>
                    <h3><b>Helsingin Medialukio / Vantaan aikuislukio</b></h3>
                    <h5>Valmistunut 2015</h5>
                    <p>Ylioppilas</p>
                  </div>
                  <div>
                    <h3><b>Metropolia University of Applied Sciences</b></h3>
                    <h5>2017 - 2020</h5>
                    <p>Tieto- ja Viestintätekniikka - Ohjemistotuotanto</p>
                  </div>
                </div>
               
              </div>
              <div className="clearer"></div>
            <hr/>
          </div>
          <div className="clearer"></div>
          <div className="footer_wrapper">
              <footer className="footer_info">
              <p>Made with NodeJS, Express Graphql backend and React app frontend.</p>
              </footer>
          </div>
        </div>

    </React.Fragment>

    );
    return content;
  }
}

export default About;
