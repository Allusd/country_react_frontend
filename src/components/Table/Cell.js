import * as React from 'react';

import './DataTable.css'

export default function Cell({
  content,
  header,
}) {

  const cellMarkup = header ? (
    <th className="cell__cell-header">
      {content}
    </th>
  ) : (
    <td className="cell">
      {content}
    </td>
  );

  return (cellMarkup);
}