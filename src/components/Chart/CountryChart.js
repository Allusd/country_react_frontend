import React from 'react';
import {Line} from 'react-chartjs'
import './CountryChart.css'


const CountryChart = props => { 
    const chartData = {
        labels:[],
        datasets: [],
        options: {
            legend: {
                display: true,
                labels: {
                    fontColor: 'rgb(255, 99, 132)'
                }
            }
    }
}
    const options = {

        ///Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines : true,
    
        //String - Colour of the grid lines
        scaleGridLineColor : "rgba(255,255,255)",
    
        //Number - Width of the grid lines
        scaleGridLineWidth : 1,
    
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
    
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
    
        //Boolean - Whether the line is curved between points
        bezierCurve : true,
    
        //Number - Tension of the bezier curve between points
        bezierCurveTension : 0.4,
    
        //Boolean - Whether to show a dot for each point
        pointDot : true,
    
        //Number - Radius of each point dot in pixels
        pointDotRadius : 4,
    
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth : 1,
    
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,
    
        //Boolean - Whether to show a stroke for datasets
        datasetStroke : true,
    
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth : 2,
    
        //Boolean - Whether to fill the dataset with a colour
        datasetFill : true,
        //String - A legend template
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"><%if(datasets[i].label){%><%=datasets[i].label%><%}%></span></li><%}%></ul>",
        
    
        //Boolean - Whether to horizontally center the label and point dot inside the grid
        offsetGridLines : false,
        legend: { fontColor: '#ffffff'  }

        
    };

    const values =[]
    const dataset = props.chartdata;
    dataset.forEach(element => {
        chartData.labels.push(element.label);
        values.push(element.data);
    });
    chartData.datasets.push({
        label: "Tons per capita",
        labelColor:"rgba(255,255,255)",
        fillColor: "rgb(9, 76, 139)",
        strokeColor: "rgba(255,255,255)",
        pointColor: "rgba(255,255,255)",
        pointStrokeColor: "rgba(255,255,255)",
        pointHighlightFill: "rgba(255,255,255)",
        pointHighlightStroke: "rgba(255,255,255)",
        data: values,
 
    })
    
return (
    <React.Fragment>
        <div className="Chart_wrapper">

                <p>Emissions (tons) per capita</p>
                <Line className="line"
                width="700px"
                height="200px" 
                data={chartData} 
                options={options}/>

        </div>
    </React.Fragment>
    )

}


export default CountryChart;