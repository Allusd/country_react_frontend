import * as React from 'react';
import './Footer.css'


export default class Footer extends React.Component {



  render() {
  
    return (
        <React.Fragment>
            <div className="footer_wrapper">
              <div className="footer_header">
              <p>Information about global warming</p>
              </div>
                  <a  rel="noopener noreferrer" target="_blank" href="https://www.wwf.org.au/what-we-do/climate/causes-of-global-warming#gs.Uli7Tz30">
                  Causes of global warming ― WWF</a>
                  <br/>
                  <a  rel="noopener noreferrer" target="_blank" href="https://en.wikipedia.org/wiki/Global_warming">
                  Global warming ― Wikipedia </a>
                  <br/>
                  <a  rel="noopener noreferrer" target="_blank" href="https://climate.nasa.gov/evidence/">
                  Evidence of global warming ― Nasa </a>
            </div>

        </React.Fragment>
    );
  }
}
