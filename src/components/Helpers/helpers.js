


const setCountryState = resData => {
    let year = 1960;
    const emIloadedCountry = [
       
        [year++, +resData.data.emissionbyCountryCode.sixty, +resData.data.populationbyCountryCode.sixty],
        [year++, +resData.data.emissionbyCountryCode.sixtyone,+resData.data.populationbyCountryCode.sixtyone],
        [year++, +resData.data.emissionbyCountryCode.sixtytwo,+resData.data.populationbyCountryCode.sixtytwo],
        [year++, +resData.data.emissionbyCountryCode.sixtythree,+resData.data.populationbyCountryCode.sixtythree],
        [year++, +resData.data.emissionbyCountryCode.sixtyfour,+resData.data.populationbyCountryCode.sixtyfour],
        [year++, +resData.data.emissionbyCountryCode.sixtyfive,+resData.data.populationbyCountryCode.sixtyfive],
        [year++, +resData.data.emissionbyCountryCode.sixtysix,+resData.data.populationbyCountryCode.sixtysix],
        [year++, +resData.data.emissionbyCountryCode.sixtyseven,+resData.data.populationbyCountryCode.sixtyseven],
        [year++, +resData.data.emissionbyCountryCode.sixtyeight,+resData.data.populationbyCountryCode.sixtyeight],
        [year++, +resData.data.emissionbyCountryCode.sixtynine,+resData.data.populationbyCountryCode.sixtynine],
        [year++, +resData.data.emissionbyCountryCode.seventy,+resData.data.populationbyCountryCode.seventy],
        [year++, +resData.data.emissionbyCountryCode.seventyone,+resData.data.populationbyCountryCode.seventyone],
        [year++, +resData.data.emissionbyCountryCode.seventytwo,+resData.data.populationbyCountryCode.seventytwo],
        [year++, +resData.data.emissionbyCountryCode.seventythree,+resData.data.populationbyCountryCode.seventythree],
        [year++, +resData.data.emissionbyCountryCode.seventyfour,+resData.data.populationbyCountryCode.seventyfour],
        [year++, +resData.data.emissionbyCountryCode.seventyfive,+resData.data.populationbyCountryCode.seventyfive],
        [year++, +resData.data.emissionbyCountryCode.seventysix,+resData.data.populationbyCountryCode.seventysix],
        [year++, +resData.data.emissionbyCountryCode.seventyseven,+resData.data.populationbyCountryCode.seventyseven],
        [year++, +resData.data.emissionbyCountryCode.seventyeight,+resData.data.populationbyCountryCode.seventyeight],
        [year++, +resData.data.emissionbyCountryCode.seventynine,+resData.data.populationbyCountryCode.seventynine],
        [year++, +resData.data.emissionbyCountryCode.eighty,+resData.data.populationbyCountryCode.eighty],
        [year++, +resData.data.emissionbyCountryCode.eightyone,+resData.data.populationbyCountryCode.eightyone],
        [year++, +resData.data.emissionbyCountryCode.eightytwo,+resData.data.populationbyCountryCode.eightytwo],
        [year++, +resData.data.emissionbyCountryCode.eightythree,+resData.data.populationbyCountryCode.eightythree],
        [year++, +resData.data.emissionbyCountryCode.eightyfour,+resData.data.populationbyCountryCode.eightyfour],
        [year++, +resData.data.emissionbyCountryCode.eightyfive,+resData.data.populationbyCountryCode.eightyfive],
        [year++, +resData.data.emissionbyCountryCode.eightysix,+resData.data.populationbyCountryCode.eightysix],
        [year++, +resData.data.emissionbyCountryCode.eightyseven,+resData.data.populationbyCountryCode.eightyseven],
        [year++, +resData.data.emissionbyCountryCode.eightyeight,+resData.data.populationbyCountryCode.eightyeight],
        [year++, +resData.data.emissionbyCountryCode.eightynine,+resData.data.populationbyCountryCode.eightynine],
        [year++, +resData.data.emissionbyCountryCode.ninety,+resData.data.populationbyCountryCode.ninety],
        [year++, +resData.data.emissionbyCountryCode.ninetyyone,+resData.data.populationbyCountryCode.ninetyyone],
        [year++, +resData.data.emissionbyCountryCode.ninetytwo,+resData.data.populationbyCountryCode.ninetytwo],
        [year++, +resData.data.emissionbyCountryCode.ninetythree,+resData.data.populationbyCountryCode.ninetythree],
        [year++, +resData.data.emissionbyCountryCode.ninetyfour,+resData.data.populationbyCountryCode.ninetyfour],
        [year++, +resData.data.emissionbyCountryCode.ninetyfive,+resData.data.populationbyCountryCode.ninetyfive],
        [year++, +resData.data.emissionbyCountryCode.ninetysix,+resData.data.populationbyCountryCode.ninetysix],
        [year++, +resData.data.emissionbyCountryCode.ninetyseven,+resData.data.populationbyCountryCode.ninetyseven],
        [year++, +resData.data.emissionbyCountryCode.ninetyeight,+resData.data.populationbyCountryCode.ninetyeight],
        [year++, +resData.data.emissionbyCountryCode.ninetynine,+resData.data.populationbyCountryCode.ninetynine],
        [year++, +resData.data.emissionbyCountryCode.twoK,+resData.data.populationbyCountryCode.twoK],
        [year++, +resData.data.emissionbyCountryCode.twoKone,+resData.data.populationbyCountryCode.twoKone],
        [year++, +resData.data.emissionbyCountryCode.twoKtwo,+resData.data.populationbyCountryCode.twoKtwo],
        [year++, +resData.data.emissionbyCountryCode.twoKthree,+resData.data.populationbyCountryCode.twoKthree],
        [year++, +resData.data.emissionbyCountryCode.twoKfour,+resData.data.populationbyCountryCode.twoKfour],
        [year++, +resData.data.emissionbyCountryCode.twoKfive,+resData.data.populationbyCountryCode.twoKfive],
        [year++, +resData.data.emissionbyCountryCode.twoKsix,+resData.data.populationbyCountryCode.twoKsix],
        [year++, +resData.data.emissionbyCountryCode.twoKseven,+resData.data.populationbyCountryCode.twoKseven],
        [year++, +resData.data.emissionbyCountryCode.twoKeight,+resData.data.populationbyCountryCode.twoKeight],
        [year++, +resData.data.emissionbyCountryCode.twoKnine,+resData.data.populationbyCountryCode.twoKnine],
        [year++, +resData.data.emissionbyCountryCode.twoKten,+resData.data.populationbyCountryCode.twoKten],
        [year++, +resData.data.emissionbyCountryCode.twoKeleven,+resData.data.populationbyCountryCode.twoKeleven],
        [year++, +resData.data.emissionbyCountryCode.twoKtwlelve,+resData.data.populationbyCountryCode.twoKtwlelve],
        [year++, +resData.data.emissionbyCountryCode.twoKthirteen,+resData.data.populationbyCountryCode.twoKthirteen],
        [year++, +resData.data.emissionbyCountryCode.twoKfourteen,+resData.data.populationbyCountryCode.twoKfourteen],
    ]
    return emIloadedCountry;
  }

  const setTableHeaders = resData => {
    const countryHeaders = ["Year", 
      resData.data.emissionbyCountryCode.IndicatorName,
      resData.data.populationbyCountryCode.IndicatorName
    ]
    return countryHeaders;
  }
  const setCountryInfo = resData => {
    const countryInfo =[
      resData.data.emissionbyCountryCode.CountryName,
      resData.data.emissionbyCountryCode.CountryCode
    ]
    return countryInfo;
  }
  const setCountryChart = resData => {
    let year = 1990;
    const chartData = [

    {label:year++,data:((+resData.data.emissionbyCountryCode.ninety*1000)/+resData.data.populationbyCountryCode.ninety).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.ninetyyone*1000)/+resData.data.populationbyCountryCode.ninetyyone).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.ninetytwo*1000)/+resData.data.populationbyCountryCode.ninetytwo).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.ninetythree*1000)/+resData.data.populationbyCountryCode.ninetythree).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.ninetyfour*1000)/+resData.data.populationbyCountryCode.ninetyfour).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.ninetyfive*1000)/+resData.data.populationbyCountryCode.ninetyfive).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.ninetysix*1000)/+resData.data.populationbyCountryCode.ninetysix).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.ninetyseven*1000)/+resData.data.populationbyCountryCode.ninetyseven).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.ninetyeight*1000)/+resData.data.populationbyCountryCode.ninetyeight).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.ninetynine*1000)/+resData.data.populationbyCountryCode.ninetynine).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoK*1000)/+resData.data.populationbyCountryCode.twoK).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKone*1000)/+resData.data.populationbyCountryCode.twoKone).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKtwo*1000)/+resData.data.populationbyCountryCode.twoKtwo).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKthree*1000)/+resData.data.populationbyCountryCode.twoKthree).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKfour*1000)/+resData.data.populationbyCountryCode.twoKfour).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKfive*1000)/+resData.data.populationbyCountryCode.twoKfive).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKsix*1000)/+resData.data.populationbyCountryCode.twoKsix).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKseven*1000)/+resData.data.populationbyCountryCode.twoKseven).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKeight*1000)/+resData.data.populationbyCountryCode.twoKeight).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKnine*1000)/+resData.data.populationbyCountryCode.twoKnine).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKten*1000)/+resData.data.populationbyCountryCode.twoKten).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKeleven*1000)/+resData.data.populationbyCountryCode.twoKeleven).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKtwlelve*1000)/+resData.data.populationbyCountryCode.twoKtwlelve).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKthirteen*1000)/+resData.data.populationbyCountryCode.twoKthirteen).toFixed(2)},
    {label:year++,data:((+resData.data.emissionbyCountryCode.twoKfourteen*1000)/+resData.data.populationbyCountryCode.twoKfourteen).toFixed(2)},
    ]
     return chartData;
  }




exports.setCountryState = setCountryState;
exports.setTableHeaders = setTableHeaders;
exports.setCountryInfo = setCountryInfo;
exports.setCountryChart = setCountryChart;