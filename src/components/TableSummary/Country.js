import React, { Component } from 'react';
import {query} from '../querys/CountryQuery'
import Spinner from '../Spinner/Spinner';
import {setCountryChart,setCountryState, setTableHeaders, setCountryInfo} from '../Helpers/helpers'

import Summary from './Summary';

class Country extends Component {
  state = {
    chartData: [],
    countryInfo: [],
    tableheaders: [],
    loadedCountry: [],
    isLoading: false
     };

     shouldComponentUpdate(nextProps, nextState) {

      return (
        nextProps.selectedCount !== this.props.selectedCount ||
        nextState.countryInfo[1] !== this.state.countryInfo[1] ||
        nextState.isLoading !== this.state.isLoading
      );
    }

  componentDidUpdate(prevProps) {

    if (prevProps.selectedCount !== this.props.selectedCount) {
      this.fetchData();
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    this.setState({ isLoading: true });
    let requestBody;

        requestBody = {
            query: query,
              variables: {
                CountryCode: this.props.selectedCount,
            }
          };
    
      fetch('https://frozen-everglades-93386.herokuapp.com/graphql', {
        method: 'POST',
        body: JSON.stringify(requestBody),
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(res => {
          if (res.status !== 200 && res.status !== 201) {
            throw new Error('Failed!');
          }
          return res.json();
        })
        .then(resData => {
            const loadedCountry = setCountryState(resData);
            const tableheaders = setTableHeaders(resData);
            const loadedCountryInfo = setCountryInfo(resData);
            const chartData = setCountryChart(resData);
            this.setState({
                chartData:chartData,
                countryInfo:loadedCountryInfo,
                tableheaders: tableheaders,
                loadedCountry:loadedCountry,
                isLoading: false });

        })
        .catch(err => {
          console.log(err);
          }
        );
  };

  componentWillUnmount() {
    console.log('Too soon...');
  }

  render() {
    let content = <Spinner/>;

    if (!this.state.isLoading && this.state.loadedCountry) {
      content = (
        <Summary
           data = {this.state.loadedCountry}
           headings = {this.state.tableheaders}
           countryInfo= {this.state.countryInfo}
           chartdata= {this.state.chartData}
        />
      );
    } else if (!this.state.isLoading && !this.state.loadedCountryInfo[1]) {
      content = <p>Failed to fetch country.</p>;
    }
    return content;
  }
}

export default Country;