import React from 'react';

import DataTable from '../Table/DataTable';

import './Summary.css';
import CountryChart from '../Chart/CountryChart';

class Summary extends React.Component {
  state = {
    outputType: 'list',
  };


  changeOutputTypeHandler = outputType => {
    if(outputType === 'list'){
      this.setState({outputType: 'list'})
    }else{
      this.setState({outputType: 'chart'})
    }

  }

  render() {

  let content = (
    <React.Fragment>
      <div className="summary_wrapper">
      <div className="summary_country">
        <h1 className="country_name">{this.props.countryInfo[0]}{" ("}{this.props.countryInfo[1]}{")"}</h1>
        {this.state.outputType === 'chart' ? (
          <button 
          className={this.state.OutputType === 'list' ? "active" : ''}
          onClick={this.changeOutputTypeHandler.bind(this, 'list')}>List</button>
          ) : (
          <button 
          className={this.state.OutputType === 'chart' ? "active" : ''}
          onClick={this.changeOutputTypeHandler.bind(this, 'chart')}>Per Capita</button>
          )}
      </div>
      <div className="summary">
      {this.state.outputType === 'list' ? (
      <DataTable headings={this.props.headings} rows={this.props.data} />
       ) : ( 
       <CountryChart className="Chart" chartdata= {this.props.chartdata}></CountryChart>
       )}
      </div>
        <div className="data_wrapper">
          <p>Data from Worldbank API</p>
          <p>Background image from www.pexels.com</p>
        </div>
      </div>
    </React.Fragment>
  )
  

  return (

    <React.Fragment>
      {content}
    </React.Fragment>
  );
};
}

export default Summary;