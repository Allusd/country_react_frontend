import React, { Component } from 'react';

import {query} from '../querys/CountrySelectQuery';
import Spinner from '../Spinner/Spinner';

import './CountrySelect.css';

class CountrySelect extends Component {
  state = { countries: [{CountryCode:"", CountryName:"", IndicatorName:"", IndicatorCode:""}], isLoading: false };

  componentDidMount() {
    this.setState({ isLoading: true });
    const requestBody = {
        query: query
      };
      fetch('https://frozen-everglades-93386.herokuapp.com/graphql', {
        method: 'POST',
        body: JSON.stringify(requestBody),
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(res => {
          if (res.status !== 200 && res.status !== 201) {
            throw new Error('Failed!');
          }
          return res.json();
        })
        .then(resData => {
          resData.data.emissions.sort((a, b) => (a.CountryName > b.CountryName) ? 1 : -1);
            this.setState({ countries: resData.data.emissions.map((country)=>({
                CountryCode: country.CountryCode,
                CountryName: country.CountryName,
                IndicatorName: country.IndicatorName,
                IndicatorCode: country.IndicatorCode
            }))
            
            
        });

        this.setState({ isLoading: false });
        })
        .catch(err => {
          console.log(err);
          }
        );
    }
  

  render() {
    let content = <Spinner className="spinner"/>;

    if (
      !this.state.isLoading &&
      this.state.countries &&
      this.state.countries.length > 0
    ) {
      content = (
        <React.Fragment>
        <div className="wrapper">
        <select
          onChange={this.props.onCountSelect}
          value={this.props.selectedCount}
        >
          {this.state.countries.map(country => (
            <option key={country.CountryCode} value={country.CountryCode}>
              {country.CountryName}
            </option>
          ))}
        </select>
        </div>
        </React.Fragment>
      );
    } else if (
      !this.state.isLoading &&
      (!this.state.countries || this.state.countries.length === 0)
    ) {
      content = <p>Could not fetch any data.</p>;
    }
    return content;
  }
}

export default CountrySelect;