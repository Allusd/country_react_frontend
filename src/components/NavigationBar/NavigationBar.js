import React from 'react';
import {NavLink} from 'react-router-dom'


import './NavigationBar.css';

const navigationBar = props =>(
    
<div className="main-navigation">
    <div className="main-navigation__logo">
        <h1>GS</h1>
    </div>
    <div>
        <nav className="main-navigation__items">
            <ul>
                <li><NavLink className="link" to="/frontpage">Frontpage</NavLink></li>
                <li><NavLink className="link" to="/about">About</NavLink></li>
            </ul>
        </nav>
    </div>
</div>

)
    


export default navigationBar;